package com.example.adonis.recyclerviewdemo;

/**
 * Created by Adonis on 7/7/2017.
 */

public class CustomObject {

    private String title;
    private String description;
    private int color;

    public CustomObject(String title, String description, int color) {
        this.title = title;
        this.description = description;
        this.color = color;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getColor() {
        return color;
    }
}
