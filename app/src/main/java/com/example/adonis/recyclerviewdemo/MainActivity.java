package com.example.adonis.recyclerviewdemo;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.adonis.recyclerviewdemo.adapter.CustomAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<CustomObject> customObjects;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeData();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        CustomAdapter adapter = new CustomAdapter(customObjects);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

    }


    private void initializeData() {
        customObjects = new ArrayList<>();
        customObjects.add(new CustomObject("Title 1", "Description 1", Color.BLUE));
        customObjects.add(new CustomObject("Title 2", "Description 2", Color.BLACK));
        customObjects.add(new CustomObject("Title 3", "Description 3", Color.WHITE));
        customObjects.add(new CustomObject("Title 4", "Description 4", Color.YELLOW));
        customObjects.add(new CustomObject("Title 5", "Description 5", Color.CYAN));
        customObjects.add(new CustomObject("Title 6", "Description 6", Color.RED));
        customObjects.add(new CustomObject("Title 7", "Description 7", Color.MAGENTA));
        customObjects.add(new CustomObject("Title 8", "Description 8", Color.GREEN));

    }
}
