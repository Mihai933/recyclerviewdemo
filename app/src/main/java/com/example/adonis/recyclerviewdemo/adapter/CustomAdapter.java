package com.example.adonis.recyclerviewdemo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.adonis.recyclerviewdemo.CustomObject;
import com.example.adonis.recyclerviewdemo.R;
import com.example.adonis.recyclerviewdemo.viewHolder.CustomViewHolder;

import java.util.List;

/**
 * Created by Adonis on 7/10/2017.
 */

public class CustomAdapter extends RecyclerView.Adapter<CustomViewHolder> {

    private List<CustomObject> customObjectList;
    private Context context;

    public CustomAdapter(List<CustomObject> customObjectList) {
        this.customObjectList = customObjectList;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        return new CustomViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, int position) {
        CustomObject customObject = customObjectList.get(position);

        holder.ivColor.setBackgroundColor(customObject.getColor());
        holder.tvDescription1.setText(customObject.getTitle());
        holder.tvDescription2.setText(customObject.getDescription());
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Position = " + holder.getPosition(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return customObjectList != null ? customObjectList.size() : 0;
    }
}
