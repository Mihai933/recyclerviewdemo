package com.example.adonis.recyclerviewdemo.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.adonis.recyclerviewdemo.R;

/**
 * Created by Adonis on 7/10/2017.
 */

public class CustomViewHolder extends RecyclerView.ViewHolder {

    public ImageView ivColor;
    public TextView tvDescription1, tvDescription2;
    public Button button;

    public CustomViewHolder(View itemView) {
        super(itemView);

        ivColor = (ImageView) itemView.findViewById(R.id.iv_color);
        tvDescription1 = (TextView) itemView.findViewById(R.id.tv_description_1);
        tvDescription2 = (TextView) itemView.findViewById(R.id.tv_description_2);
        button = (Button) itemView.findViewById(R.id.btn);
    }
}
